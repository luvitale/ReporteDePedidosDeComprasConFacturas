*&---------------------------------------------------------------------*
*&  Include           YLN_ALV_SCR
*&---------------------------------------------------------------------*

*&---------------------------------------------------------------------*
*& SELECTION-SCREEN
*&---------------------------------------------------------------------*
SELECTION-SCREEN BEGIN OF BLOCK b01 WITH FRAME TITLE text-b01.

SELECT-OPTIONS:
  s_ebeln FOR ekko-ebeln NO INTERVALS, " Pedido de Compras
  s_bukrs FOR ekko-bukrs NO INTERVALS, " Sociedad
  s_bsart FOR ekko-bsart NO INTERVALS, " Clase de Documento
  s_lifnr FOR ekko-lifnr NO INTERVALS, " Proveedor
  s_matnr FOR ekpo-matnr NO INTERVALS, " Material
  s_waers FOR ekko-waers NO INTERVALS. " Moneda

SELECTION-SCREEN END OF BLOCK b01.