*&---------------------------------------------------------------------*
*&  Include           YLN_ALV_F01
*&---------------------------------------------------------------------*

*&---------------------------------------------------------------------*
*&      Form  GET_DATA
*&---------------------------------------------------------------------*
FORM get_data
  CHANGING ch_t_alv TYPE tyt_alv.

  DATA:
    lt_ekko   TYPE tyt_ekko,
    lt_ekpo   TYPE tyt_ekpo,
    lt_rbkp   TYPE tyt_rbkp,
    lt_rseg   TYPE tyt_rseg,
    lt_makt   TYPE tyt_makt,
    lt_t161t  TYPE tyt_t161t,
    lt_dd07v  TYPE tyt_dd07v,
    lt_lfa1   TYPE tyt_lfa1.

  PERFORM get_data_db
    CHANGING
      lt_ekko[]
      lt_ekpo[]
      lt_rbkp[]
      lt_rseg[]
      lt_makt[]
      lt_t161t[]
      lt_dd07v[]
      lt_lfa1[].

  PERFORM process_data
    USING
      lt_ekko[]
      lt_ekpo[]
      lt_rbkp[]
      lt_rseg[]
      lt_makt[]
      lt_t161t[]
      lt_dd07v[]
      lt_lfa1[]
    CHANGING
      ch_t_alv[].

ENDFORM.                    " GET_DATA

*&---------------------------------------------------------------------*
*&      Form  GET_DATA_DB
*&---------------------------------------------------------------------*
FORM get_data_db
  CHANGING
    ch_t_ekko   TYPE tyt_ekko
    ch_t_ekpo   TYPE tyt_ekpo
    ch_t_rbkp   TYPE tyt_rbkp
    ch_t_rseg   TYPE tyt_rseg
    ch_t_makt   TYPE tyt_makt
    ch_t_t161t  TYPE tyt_t161t
    ch_t_dd07v  TYPE tyt_dd07v
    ch_t_lfa1   TYPE tyt_lfa1.

  CALL FUNCTION 'DD_DOMVALUES_GET'
    EXPORTING
      domname        = 'EBSTYP'
      text           = 'X'
      langu          = sy-langu
    TABLES
      dd07v_tab      = ch_t_dd07v
    EXCEPTIONS
      wrong_textflag = 1
      OTHERS         = 2.

  IF sy-subrc <> 0.
    MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
            WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
  ENDIF.

  SELECT ebeln bukrs bstyp bsart lifnr waers
    FROM ekko
    INTO TABLE ch_t_ekko
  WHERE ebeln IN s_ebeln
    AND bukrs IN s_bukrs
    AND bsart IN s_bsart
    AND lifnr IN s_lifnr
    AND waers IN s_waers.

  IF ch_t_ekko[] IS NOT INITIAL.

    SELECT bsart bstyp spras batxt
      FROM t161t
      INTO TABLE ch_t_t161t
      FOR ALL ENTRIES IN ch_t_ekko
    WHERE bsart EQ ch_t_ekko-bsart
      AND bstyp EQ ch_t_ekko-bstyp
      AND spras EQ sy-langu.

    SELECT ebeln ebelp matnr werks lgort menge meins netwr
      FROM ekpo
      INTO TABLE ch_t_ekpo
      FOR ALL ENTRIES IN ch_t_ekko
    WHERE ebeln EQ ch_t_ekko-ebeln
      AND matnr IN s_matnr.

    IF ch_t_ekpo[] IS NOT INITIAL.

      SELECT matnr spras maktx
        FROM makt
        INTO TABLE ch_t_makt
        FOR ALL ENTRIES IN ch_t_ekpo
      WHERE matnr EQ ch_t_ekpo-matnr
        AND spras EQ sy-langu.

      SELECT belnr gjahr buzei ebeln ebelp
        FROM rseg
        INTO TABLE ch_t_rseg
        FOR ALL ENTRIES IN ch_t_ekpo
      WHERE ebeln EQ ch_t_ekpo-ebeln
        AND ebelp EQ ch_t_ekpo-ebelp.

      IF ch_t_rseg[] IS NOT INITIAL.

        SELECT belnr gjahr blart xblnr lifnr
          FROM rbkp
          INTO TABLE ch_t_rbkp
          FOR ALL ENTRIES IN ch_t_rseg
        WHERE belnr EQ ch_t_rseg-belnr
          AND gjahr EQ ch_t_rseg-gjahr.

        IF ch_t_rbkp[] IS NOT INITIAL.

          SELECT lifnr name1
            FROM lfa1
            INTO TABLE ch_t_lfa1
            FOR ALL ENTRIES IN ch_t_rbkp
          WHERE lifnr EQ ch_t_rbkp-lifnr.

        ENDIF.

      ENDIF.

    ENDIF.

  ENDIF.

ENDFORM.                    " GET_DATA_DB

*&---------------------------------------------------------------------*
*&      Form  PROCESS_DATA
*&---------------------------------------------------------------------*
FORM process_data
  USING
    us_t_ekko   TYPE tyt_ekko
    us_t_ekpo   TYPE tyt_ekpo
    us_t_rbkp   TYPE tyt_rbkp
    us_t_rseg   TYPE tyt_rseg
    us_t_makt   TYPE tyt_makt
    us_t_t161t  TYPE tyt_t161t
    us_t_dd07v  TYPE tyt_dd07v
    us_t_lfa1   TYPE tyt_lfa1
  CHANGING
    ch_t_alv   TYPE tyt_alv.

  FIELD-SYMBOLS:
    <lfs_ekko>      TYPE ty_ekko,
    <lfs_ekpo>      TYPE ty_ekpo,
    <lfs_rbkp>      TYPE ty_rbkp,
    <lfs_rseg>      TYPE ty_rseg,
    <lfs_makt>      TYPE ty_makt,
    <lfs_t161t>     TYPE ty_t161t,
    <lfs_lfa1>      TYPE ty_lfa1,
    <lfs_dd07v>     TYPE dd07v,
    <lfs_alv>       TYPE ty_alv.


  LOOP AT us_t_ekko ASSIGNING <lfs_ekko>.

    READ TABLE us_t_t161t ASSIGNING <lfs_t161t>
      WITH KEY bsart = <lfs_ekko>-bsart.

    READ TABLE us_t_dd07v ASSIGNING <lfs_dd07v>
      WITH KEY domvalue_l = <lfs_ekko>-bstyp.

    LOOP AT us_t_ekpo ASSIGNING <lfs_ekpo>
      WHERE ebeln EQ <lfs_ekko>-ebeln.

      READ TABLE us_t_makt ASSIGNING <lfs_makt>
        WITH KEY matnr = <lfs_ekpo>-matnr.

      LOOP AT us_t_rseg ASSIGNING <lfs_rseg>
        WHERE ebeln EQ <lfs_ekpo>-ebeln
          AND ebelp EQ <lfs_ekpo>-ebelp.

        READ TABLE us_t_rbkp ASSIGNING <lfs_rbkp>
          WITH KEY belnr = <lfs_rseg>-belnr
                   gjahr = <lfs_rseg>-gjahr.

        READ TABLE us_t_lfa1 ASSIGNING <lfs_lfa1>
          WITH KEY lifnr = <lfs_rbkp>-lifnr.

        APPEND INITIAL LINE TO ch_t_alv ASSIGNING <lfs_alv>.

        IF <lfs_alv> IS ASSIGNED.

          IF <lfs_ekko> IS ASSIGNED.

            <lfs_alv>-ebeln = <lfs_ekko>-ebeln.
            <lfs_alv>-bukrs = <lfs_ekko>-bukrs.
            <lfs_alv>-bstyp = <lfs_ekko>-bstyp.
            <lfs_alv>-bsart = <lfs_ekko>-bsart.
            <lfs_alv>-lifnr = <lfs_ekko>-lifnr.
            <lfs_alv>-waers = <lfs_ekko>-waers.

            IF <lfs_t161t> IS ASSIGNED.

              <lfs_alv>-bsart_desc = <lfs_t161t>-batxt.

            ENDIF.

            IF <lfs_dd07v> IS ASSIGNED.

              <lfs_alv>-bstyp_desc = <lfs_dd07v>-ddtext.

            ENDIF.

            IF <lfs_ekpo> IS ASSIGNED.

              <lfs_alv>-ebelp = <lfs_ekpo>-ebelp.
              <lfs_alv>-matnr = <lfs_ekpo>-matnr.
              <lfs_alv>-werks = <lfs_ekpo>-werks.
              <lfs_alv>-lgort = <lfs_ekpo>-lgort.
              <lfs_alv>-menge = <lfs_ekpo>-menge.
              <lfs_alv>-meins = <lfs_ekpo>-meins.
              <lfs_alv>-netwr = <lfs_ekpo>-netwr.

              IF <lfs_makt> IS ASSIGNED.

                <lfs_alv>-matnr_desc = <lfs_makt>-maktx.

              ENDIF.

              IF <lfs_rseg> IS ASSIGNED.

                IF <lfs_rbkp> IS ASSIGNED.

                  <lfs_alv>-belnr = <lfs_rbkp>-belnr.
                  <lfs_alv>-gjahr = <lfs_rbkp>-gjahr.
                  <lfs_alv>-blart = <lfs_rbkp>-blart.
                  <lfs_alv>-xblnr = <lfs_rbkp>-xblnr.

                  IF <lfs_lfa1> IS ASSIGNED.

                    <lfs_alv>-lifnr_name = <lfs_lfa1>-name1.

                  ENDIF.

                ENDIF.

              ENDIF.

            ENDIF.
          ENDIF.
        ENDIF.
      ENDLOOP.
    ENDLOOP.
  ENDLOOP.

ENDFORM.                    " PROCESS_DATA

*&---------------------------------------------------------------------*
*&      Form  SHOW_REPORT
*&---------------------------------------------------------------------*
FORM show_report
  USING us_t_alv TYPE tyt_alv.

  DATA:
    le_layout           TYPE slis_layout_alv,
    lt_fieldcat         TYPE slis_t_fieldcat_alv,
    lv_callback_program TYPE sy-repid,
    lt_sort             TYPE slis_t_sortinfo_alv.

  lv_callback_program = sy-repid.

  PERFORM set_layout
    CHANGING
      le_layout.

  PERFORM set_fieldcat
    CHANGING
      lt_fieldcat[].

  PERFORM set_sort
    CHANGING
      lt_sort[].

  SORT us_t_alv.

  CALL FUNCTION 'REUSE_ALV_GRID_DISPLAY'
    EXPORTING
      i_callback_program = lv_callback_program
      is_layout          = le_layout
      it_fieldcat        = lt_fieldcat
      i_callback_pf_status_set = 'SET_STATUS_ALV'
      i_callback_user_command  = 'ALV_USER_COMMAND'
      it_sort            = lt_sort
    TABLES
      t_outtab           = us_t_alv
    EXCEPTIONS
      program_error      = 1
      OTHERS             = 2.

  IF sy-subrc <> 0.
    MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
            WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
  ENDIF.

ENDFORM.                    " SHOW_REPORT