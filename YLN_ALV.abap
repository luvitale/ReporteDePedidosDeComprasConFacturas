*&---------------------------------------------------------------------*
*& Report  YLN_ALV
*&---------------------------------------------------------------------*

REPORT  yln_alv.

*&---------------------------------------------------------------------*
*& INCLUDE
*&---------------------------------------------------------------------*
INCLUDE:
  yln_alv_top,
  yln_alv_scr,
  yln_alv_f01,
  yln_alv_f02,
  yln_alv_dld_top,
  yln_alv_dld_scr,
  yln_alv_dld.

*&---------------------------------------------------------------------*
*& END-OF-SELECTION
*&---------------------------------------------------------------------*
END-OF-SELECTION.

  PERFORM show_purchase_orders
    USING
      t_alv[].

*&---------------------------------------------------------------------*
*&      Form  SHOW_PURCHASE_ORDERS
*&---------------------------------------------------------------------*
FORM show_purchase_orders
  USING
    us_t_alv TYPE tyt_alv.

  PERFORM get_data
    CHANGING
      t_alv[].

  IF t_alv[] IS NOT INITIAL.

    PERFORM show_report
      USING
        t_alv[].

  ELSE.

    MESSAGE 'No hay datos para los par�metros de selecci�n'(e01) TYPE 'S' DISPLAY LIKE 'E'.

  ENDIF.

ENDFORM.                    " SHOW_PURCHASE_ORDERS