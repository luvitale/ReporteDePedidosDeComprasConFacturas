*----------------------------------------------------------------------*
***INCLUDE YLN_ALV_DLD
*----------------------------------------------------------------------*

*&---------------------------------------------------------------------*
*&      Form  DOWNLOAD_LINES_TO_FILE
*&---------------------------------------------------------------------*
FORM download_lines_to_file
  USING
    us_t_alv TYPE tyt_alv.

  DATA:
    lv_filename TYPE string,
    lt_file     TYPE tyt_string.

  CALL SELECTION-SCREEN 1001.

  PERFORM get_csv_filename
    USING
      p_fileo
    CHANGING
      lv_filename.

  PERFORM format_out_file
    USING
      us_t_alv[]
    CHANGING
      lt_file[].

  PERFORM download_file_to_pc
    USING
      lv_filename
      lt_file[].

ENDFORM.                    " DOWNLOAD_LINES_TO_FILE

*&---------------------------------------------------------------------*
*&      Form  GET_CSV_FILENAME
*&---------------------------------------------------------------------*
FORM get_csv_filename
  USING
    us_v_file
  CHANGING
    ch_v_filename.

  CONCATENATE us_v_file
              '/'
              'Pedidos'
              sy-datum
              sy-uzeit INTO ch_v_filename.

  CONCATENATE ch_v_filename '.csv' INTO ch_v_filename.

ENDFORM.                    " GET_CSV_FILENAME

*&---------------------------------------------------------------------*
*&      Form  FORMAT_OUT_FILE
*&---------------------------------------------------------------------*
FORM format_out_file
  USING
    us_t_alv TYPE tyt_alv
  CHANGING
    ch_t_file TYPE tyt_string.

  DATA:
    lt_selected_alv TYPE tyt_alv,
    le_alv          TYPE ty_alv,
    lv_string       TYPE string.

  LOOP AT us_t_alv INTO le_alv
    WHERE sel EQ abap_true.

    CLEAR lv_string.

    PERFORM format_out_csv_line
      USING
        le_alv
      CHANGING
        lv_string.

    APPEND lv_string TO ch_t_file.

  ENDLOOP.

ENDFORM.                    " FORMAT_OUT_FILE

*&---------------------------------------------------------------------*
*&      Form  FORMAT_OUT_CSV_LINE
*&---------------------------------------------------------------------*
FORM format_out_csv_line
  USING
    us_e_alv TYPE ty_alv
  CHANGING
    ch_v_string.

  DATA:
    lv_menge_aux TYPE c LENGTH 13,
    lv_netwr_aux TYPE c LENGTH 13.

  CALL FUNCTION 'CONVERSION_EXIT_ALPHA_OUTPUT'
    EXPORTING
      input  = us_e_alv-ebeln
    IMPORTING
      output = us_e_alv-ebeln.

  CONDENSE us_e_alv-ebeln.

  CALL FUNCTION 'CONVERSION_EXIT_ALPHA_OUTPUT'
    EXPORTING
      input  = us_e_alv-ebelp
    IMPORTING
      output = us_e_alv-ebelp.

  CONDENSE us_e_alv-ebelp.

  CALL FUNCTION 'CONVERSION_EXIT_ALPHA_OUTPUT'
    EXPORTING
      input  = us_e_alv-lifnr
    IMPORTING
      output = us_e_alv-lifnr.

  CONDENSE us_e_alv-lifnr.

  CALL FUNCTION 'CONVERSION_EXIT_ALPHA_OUTPUT'
    EXPORTING
      input  = us_e_alv-matnr
    IMPORTING
      output = us_e_alv-matnr.

  CONDENSE us_e_alv-matnr.

  lv_menge_aux = us_e_alv-menge.
  REPLACE '.' IN lv_menge_aux WITH ','.
  CONDENSE lv_menge_aux.

  CALL FUNCTION 'CONVERSION_EXIT_ALPHA_OUTPUT'
    EXPORTING
      input  = us_e_alv-belnr
    IMPORTING
      output = us_e_alv-belnr.

  CONDENSE us_e_alv-belnr.

  lv_netwr_aux = us_e_alv-netwr.
  REPLACE '.' IN lv_netwr_aux WITH ','.
  CONDENSE lv_netwr_aux.

  CONCATENATE us_e_alv-ebeln
              us_e_alv-ebelp
              us_e_alv-bukrs
              us_e_alv-bstyp
              us_e_alv-bsart
              us_e_alv-lifnr
              us_e_alv-matnr
              us_e_alv-werks
              us_e_alv-lgort
              lv_menge_aux
              us_e_alv-meins
              us_e_alv-belnr
              us_e_alv-gjahr
              us_e_alv-blart
              us_e_alv-xblnr
              lv_netwr_aux
              us_e_alv-waers
    INTO ch_v_string SEPARATED BY ';'.

ENDFORM.                    " FORMAT_OUT_CSV_LINE

*&---------------------------------------------------------------------*
*&      Form  DOWNLOAD_FILE_TO_PC
*&---------------------------------------------------------------------*
FORM download_file_to_pc
  USING
    us_v_filename
    us_t_file     TYPE tyt_string.

  DATA:
    lv_filename TYPE string.

  lv_filename = us_v_filename.

  CALL METHOD cl_gui_frontend_services=>gui_download
    EXPORTING
      filename                = lv_filename
    CHANGING
      data_tab                = us_t_file
    EXCEPTIONS
      file_write_error        = 1
      no_batch                = 2
      gui_refuse_filetransfer = 3
      invalid_type            = 4
      no_authority            = 5
      unknown_error           = 6
      header_not_allowed      = 7
      separator_not_allowed   = 8
      filesize_not_allowed    = 9
      header_too_long         = 10
      dp_error_create         = 11
      dp_error_send           = 12
      dp_error_write          = 13
      unknown_dp_error        = 14
      access_denied           = 15
      dp_out_of_memory        = 16
      disk_full               = 17
      dp_timeout              = 18
      file_not_found          = 19
      dataprovider_exception  = 20
      control_flush_error     = 21
      not_supported_by_gui    = 22
      error_no_gui            = 23
      OTHERS                  = 24.

  IF sy-subrc <> 0.
    MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
               WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
  ENDIF.

ENDFORM.                    " DOWNLOAD_FILE_TO_PC