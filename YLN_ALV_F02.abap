*----------------------------------------------------------------------*
***INCLUDE YLN_ALV_F02
*----------------------------------------------------------------------*

*&---------------------------------------------------------------------*
*&      Form  SET_LAYOUT
*&---------------------------------------------------------------------*
FORM set_layout
  CHANGING
    ch_e_layout TYPE slis_layout_alv.

  ch_e_layout-colwidth_optimize = abap_true.
  ch_e_layout-zebra = abap_true.
  ch_e_layout-box_fieldname = 'SEL'.

ENDFORM.                    " SET_LAYOUT

*&---------------------------------------------------------------------*
*&      Form  SET_FIELDCAT
*&---------------------------------------------------------------------*
FORM set_fieldcat
  CHANGING
    ch_t_fieldcat TYPE slis_t_fieldcat_alv.

  DATA:
    le_fieldcat TYPE LINE OF slis_t_fieldcat_alv.

  PERFORM set_fieldcat_field
    USING
      'T_ALV'
      'EBELN'
      'Pedido de compras'(f01)
      abap_false
    CHANGING
      ch_t_fieldcat[].

  PERFORM set_fieldcat_field
    USING
      'T_ALV'
      'EBELP'
      'Posici�n'(f02)
      abap_false
    CHANGING
      ch_t_fieldcat[].

  PERFORM set_fieldcat_field
    USING
      'T_ALV'
      'BUKRS'
      'Sociedad'(f03)
      abap_false
    CHANGING
      ch_t_fieldcat[].

  PERFORM set_fieldcat_field
    USING
      'T_ALV'
      'BSTYP'
      'Tipo documento'(f04)
      abap_false
    CHANGING
      ch_t_fieldcat[].

  PERFORM set_fieldcat_field
    USING
      'T_ALV'
      'BSTYP_DESC'
      'Descripci�n del Tipo de Documento'(f05)
      abap_false
    CHANGING
      ch_t_fieldcat[].

  PERFORM set_fieldcat_field
    USING
      'T_ALV'
      'BSART'
      'Clase documento'(f06)
      abap_false
    CHANGING
      ch_t_fieldcat[].

  PERFORM set_fieldcat_field
    USING
      'T_ALV'
      'BSART_DESC'
      'Descripci�n de la Clase de Documento'(f07)
      abap_false
    CHANGING
      ch_t_fieldcat[].

  PERFORM set_fieldcat_field
    USING
      'T_ALV'
      'LIFNR'
      'Proveedor'(f08)
      abap_false
    CHANGING
      ch_t_fieldcat[].

  PERFORM set_fieldcat_field
    USING
      'T_ALV'
      'LIFNR_NAME'
      'Nombre del Proveedor'(f09)
      abap_false
    CHANGING
      ch_t_fieldcat[].

  PERFORM set_fieldcat_field
    USING
      'T_ALV'
      'MATNR'
      'Material'(f10)
      abap_false
    CHANGING
      ch_t_fieldcat[].

  PERFORM set_fieldcat_field
    USING
      'T_ALV'
      'MATNR_DESC'
      'Descripci�n del Material'(f11)
      abap_false
    CHANGING
      ch_t_fieldcat[].

  PERFORM set_fieldcat_field
    USING
      'T_ALV'
      'WERKS'
      'Centro'(f12)
      abap_false
    CHANGING
      ch_t_fieldcat[].

  PERFORM set_fieldcat_field
    USING
      'T_ALV'
      'LGORT'
      'Almac�n'(f13)
      abap_false
    CHANGING
      ch_t_fieldcat[].

  PERFORM set_fieldcat_field
    USING
      'T_ALV'
      'MENGE'
      'Cantidad'(f14)
      abap_false
    CHANGING
      ch_t_fieldcat[].

  PERFORM set_fieldcat_field
    USING
      'T_ALV'
      'MEINS'
      'Unidad'(f15)
      abap_false
    CHANGING
      ch_t_fieldcat[].

  PERFORM set_fieldcat_field
    USING
      'T_ALV'
      'BELNR'
      'Factura'(f16)
      abap_false
    CHANGING
      ch_t_fieldcat[].

  PERFORM set_fieldcat_field
    USING
      'T_ALV'
      'GJAHR'
      'Ejercicio'(f17)
      abap_false
    CHANGING
      ch_t_fieldcat[].

  PERFORM set_fieldcat_field
    USING
      'T_ALV'
      'BLART'
      'Clase de Doc.'(f18)
      abap_false
    CHANGING
      ch_t_fieldcat[].

  PERFORM set_fieldcat_field
    USING
      'T_ALV'
      'XBLNR'
      'Nro Legal'(f19)
      abap_false
    CHANGING
      ch_t_fieldcat[].

  PERFORM set_fieldcat_field
    USING
      'T_ALV'
      'NETWR'
      'Importe'(f20)
      abap_true
    CHANGING
      ch_t_fieldcat[].

  PERFORM set_fieldcat_field
    USING
      'T_ALV'
      'WAERS'
      'Moneda'(f21)
      abap_false
    CHANGING
      ch_t_fieldcat[].

ENDFORM.                    " SET_FIELDCAT

*&---------------------------------------------------------------------*
*&      Form  SET_FIELDCAT_FIELD
*&---------------------------------------------------------------------*
FORM set_fieldcat_field
  USING
    us_v_tabname
    us_v_field_name
    us_v_text
    us_v_do_sum
  CHANGING
    ch_t_fieldcat TYPE slis_t_fieldcat_alv.

  DATA:
    le_fieldcat TYPE LINE OF slis_t_fieldcat_alv.

  CLEAR le_fieldcat.

  le_fieldcat-tabname   = us_v_tabname.
  le_fieldcat-fieldname = us_v_field_name.
  le_fieldcat-seltext_l = us_v_text.
  le_fieldcat-do_sum    = us_v_do_sum.

  APPEND le_fieldcat TO ch_t_fieldcat.

ENDFORM.                    " SET_FIELDCAT_FIELD

*&---------------------------------------------------------------------*
*&      Form  SET_SORT
*&---------------------------------------------------------------------*
FORM set_sort
  CHANGING
    ch_t_sort TYPE slis_t_sortinfo_alv.

  DATA:
    le_sort TYPE LINE OF slis_t_sortinfo_alv.

  le_sort-tabname   = 'T_ALV'.
  le_sort-fieldname = 'EBELN'.
  le_sort-up        = abap_true.
  le_sort-subtot    = abap_true.

  APPEND le_sort TO ch_t_sort.

ENDFORM.                    " SET_SORT

*&---------------------------------------------------------------------*
*&      Form  SET_STATUS_ALV
*&---------------------------------------------------------------------*
FORM set_status_alv
  USING
    us_t_extab TYPE slis_t_extab.

  SET PF-STATUS 'STATUS_ALV' EXCLUDING us_t_extab.

ENDFORM.                    " SET_STATUS_ALV

*&---------------------------------------------------------------------*
*&      Form  ALV_USER_COMMAND
*&---------------------------------------------------------------------*
FORM alv_user_command
  USING
    us_v_ucomm    TYPE sy-ucomm
    us_e_selfield TYPE slis_selfield.

  IF us_v_ucomm EQ 'DLD'.

    PERFORM download_lines_to_file
      USING
        t_alv[].

  ENDIF.

ENDFORM.                    " ALV_USER_COMMAND