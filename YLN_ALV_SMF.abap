*----------------------------------------------------------------------*
***INCLUDE YLN_ALV_SMF
*----------------------------------------------------------------------*

*&---------------------------------------------------------------------*
*&      Form  SHOW_SMARTFORM
*&---------------------------------------------------------------------*
FORM show_smartform
  USING
    us_t_alv TYPE tyt_alv.

  DATA:
    lt_selected_alv       TYPE tyt_alv,
    le_alv                TYPE ty_alv,
    lv_lines_selected_alv TYPE i,
    ls_selected_alv       TYPE ty_alv.

  LOOP AT us_t_alv INTO le_alv
    WHERE sel EQ abap_true.

    APPEND le_alv TO lt_selected_alv.

  ENDLOOP.

  DESCRIBE TABLE lt_selected_alv LINES lv_lines_selected_alv.

  IF lv_lines_selected_alv <> 1.

    MESSAGE 'Es necesario seleccionar 1 elemento'(e02)  TYPE 'S' DISPLAY LIKE 'E'.

  ELSE.

    PERFORM show_smartform_of_selection
      USING
        lt_selected_alv[]
        t_alv[].

  ENDIF.

ENDFORM.                    " SHOW_SMARTFORM

*&---------------------------------------------------------------------*
*&      Form  SHOW_SMARTFORM_OF_SELECTION
*&---------------------------------------------------------------------*
FORM show_smartform_of_selection
  USING
    us_t_selected_alv TYPE tyt_alv
    us_t_alv          TYPE tyt_alv.

  DATA:
    ls_header         TYPE yln_header,
    ls_provider       TYPE yln_provider,
    lt_position       TYPE yln_t_position,
    ls_position       TYPE yln_position,
    ls_total          TYPE yln_total,
    lv_fm_name        TYPE rs38l_fnam,
    ls_alv            TYPE ty_alv,
    ls_selected_alv   TYPE ty_alv.

  READ TABLE us_t_selected_alv INTO ls_selected_alv INDEX 1.

  ls_header-ebeln         = ls_selected_alv-ebeln.
  ls_header-bukrs         = ls_selected_alv-bukrs.
  ls_header-bsart         = ls_selected_alv-bsart.
  ls_header-bsart_desc    = ls_selected_alv-bsart_desc.

  ls_provider-lifnr       = ls_selected_alv-lifnr.
  ls_provider-lifnr_name  = ls_selected_alv-lifnr_name.

  LOOP AT us_t_alv INTO ls_alv
    WHERE ebeln EQ ls_selected_alv-ebeln.

    CLEAR ls_position.

    ls_position-matnr      = ls_alv-matnr.
    ls_position-matnr_desc = ls_alv-matnr_desc.
    ls_position-netwr      = ls_alv-netwr.
    ls_position-waers      = ls_alv-waers.

    APPEND ls_position TO lt_position.

    ADD ls_position-netwr TO ls_total-netwr.

  ENDLOOP.

  CALL FUNCTION 'SSF_FUNCTION_MODULE_NAME'
    EXPORTING
      formname           = 'YLN_FORM'
    IMPORTING
      fm_name            = lv_fm_name
    EXCEPTIONS
      no_form            = 1
      no_function_module = 2
      OTHERS             = 3.

  IF sy-subrc EQ 0.

    CALL FUNCTION lv_fm_name
      EXPORTING
        header           = ls_header
        provider         = ls_provider
        position         = lt_position
        total            = ls_total
      EXCEPTIONS
        formatting_error = 1
        internal_error   = 2
        send_error       = 3
        user_canceled    = 4
        OTHERS           = 5.

  ELSE.

    MESSAGE 'No se pudo crear el Smartform'(e03) TYPE 'S' DISPLAY LIKE 'E'.

  ENDIF.

ENDFORM.                    " SHOW_SMARTFORM_OF_SELECTION