# Reporte de Pedidos de Compras con Facturas

## Academia ABAP

### Examen Final

#### Contenido
* [Requerimiento](#requerimiento)
* [Filtros](#filtros)
* [Reporte](#reporte)
    * [Características](#características)
* [Formato del Archivo](#formato-del-archivo)
* [Diseño del Formulario](#diseño-del-formulario)
* [Diseño de la Pantalla](#diseño-de-la-pantalla)
* [Tabla de BD](#tabla-de-bd)
    * [Relación](#relación)

#### Requerimiento

Se necesita un Reporte ALV de Pedidos de Compras (`EKKO`), con sus Facturas asociadas (`RBKP`), con las
siguientes características.

#### Filtros

(Ver la sección [Reporte](#reporte) para tener referencia del campo técnico)

- Pedido de Compras
- Sociedad
- Clase de Documento
- Proveedor
- Material
- Moneda

#### Reporte

(La relación entre tablas se muestra en la sección [Relación](#relación))

| Campo          | Descripción                          | Lógica                                             |
|----------------|--------------------------------------|----------------------------------------------------|
| EKKO-EBELN     | Pedido de Compras                    |                                                    |
| EKPO-EBELP     | Posición                             |                                                    |
| EKKO-BUKRS     | Sociedad                             |                                                    |
| EKKO-BSTYP     | Tipo documento                       |                                                    |
| Tabla de Texto | Descripción del Tipo de Documento    |                                                    |
| EKKO-BSART     | Clase documento                      |                                                    |
| Tabla de Texto | Descripción de la Clase de Documento |                                                    |
| EKKO-LIFNR     | Proveedor                            |                                                    |
| Lógica         | Nombre del Proveedor                 | LFA1-NAME1, ingresando con LFA1-LIFNR = RBKP-LIFNR |
| EKPO-MATNR     | Material                             |                                                    |
| Lógica         | Descripción del Material             | MAKT-MAKTX, ingresando con MAKT-MATNR = EKPO-MATNR |
| EKPO-WERKS     | Centro                               |                                                    |
| EKPO-LGORT     | Almacén                              |                                                    |
| EKPO-MENGE     | Cantidad                             |                                                    |
| EKPO-MEINS     | Unidad                               |                                                    |
| RBKP-BELNR     | Factura                              |                                                    |
| RBKP-GJAHR     | Ejercicio                            |                                                    |
| RBKP-BLART     | Clase de Doc.                        |                                                    |
| RBKP-XBLNR     | Nro Legal                            |                                                    |
| EKPO-NETWR     | Importe                              |                                                    |
| EKKO-WAERS     | Moneda                               |                                                    |

##### Características

* Debe tener el campo Importe subtotalizado por Pedido de Compras.
* Debe tener un botón que permita descargar las líneas seleccionados a un archivo de la PC Local con el Formato descripto en la sección [Formato del Archivo](#formato-del-archivo).
* Debe tener un botón (que solo permita seleccionar una línea) y muestre un Formulario Smartform con el diseño descripto en la sección [Diseño del Formulario](#diseño-del-formulario).
* Debe tener un botón (que solo permita seleccionar una línea) que muestre una nueva Pantalla (Dynpro) según el diseño descripto en la sección [Diseño de la Pantalla](#diseño-de-la-pantalla).
* La misma debe permitir el ingreso de una Observación y posterior guardado (mediante un botón Guardar) en una tabla de la base de datos descripta en la sección [Tabla de BD](#tabla-de-bd).
* Al presionar el botón Volver en esta pantalla, debemos volver al Reporte.

#### Formato del Archivo

(CSV, separado por ;)

| Campo      | Formato                             |
|------------|-------------------------------------|
| EKKO-EBELN | Sin ceros a la izquierda            |
| EKPO-EBELP | Sin ceros a la izquierda            |
| EKKO-BUKRS |                                     |
| EKKO-BSTYP |                                     |
| EKKO-BSART |                                     |
| EKKO-LIFNR | Sin ceros a la izquierda            |
| EKPO-MATNR | Sin ceros a la izquierda            |
| EKPO-WERKS |                                     |
| EKPO-LGORT |                                     |
| EKPO-MENGE | Con coma (,) como separador decimal |
| EKPO-MEINS |                                     |
| RBKP-BELNR | Sin ceros a la izquierda            |
| RBKP-GJAHR |                                     |
| RBKP-BLART |                                     |
| RBKP-XBLNR |                                     |
| EKPO-NETWR | Con coma (,) como separador decimal |
| EKKO-WAERS |                                     |

#### Diseño del Formulario

Tamaño de hoja: A4

* En la tabla Main, con las Posiciones, se deben mostrar TODAS las posiciones del Documento seleccionado (aunque solo se haya seleccionado una posición)
* La columna Material y Descripción deben estar alineadas a la izquierda.
* La columna Importe debe estar alineada a la derecha.
* La columna Moneda debe estar centrada.


#### Diseño de la Pantalla

Para que una Pantalla/Dynpro no sea de Pantalla Completa y sí sea popup/ventana emergente, hay que
llamar a la pantalla con la setencia:

```abap
CALL SCREEN numeroPantalla STARTING AT xInicial yInicial ENDING AT xFinal yFinal.
```

#### Tabla de BD

| Campo       | Tipo de Datos | Clave | Descripción    |
|-------------|---------------|-------|----------------|
| EBELN       | EKKO-EBELN    | Sí    | Pedido Compras |
| EBELP       | EKPO-EBELP    | Sí    | Posición       |
| OBSERVACIÓN | STRING        | No    | Observación    |

Recordar que las tablas siempre deben tener el Mandate (MANDT).


##### Relación

***EKKO > EKPO*** (1 a N)

```abap
EKKO-EBELN = EKPO-EBELN
```

***EKPO > RSEG*** (1 a N)

```abap
EKPO-EBELN = RSEG-EBELN
EKPO-EBELP = RSEG-EBELP
```

***RSEG > RBKP*** (N a 1)

```abap
RSEG-BELNR = RBKP-BELNR
RSEG-GJAHR = RBKP-GJAHR
```
