*&---------------------------------------------------------------------*
*&  Include           YLN_ALV_DLD_SCR
*&---------------------------------------------------------------------*

*&---------------------------------------------------------------------*
*& SELECTION-SCREEN
*&---------------------------------------------------------------------*
SELECTION-SCREEN BEGIN OF SCREEN 1001.

SELECTION-SCREEN BEGIN OF BLOCK b02 WITH FRAME TITLE text-002.

PARAMETERS:
  p_fileo TYPE string LOWER CASE.

SELECTION-SCREEN END OF BLOCK b02.

SELECTION-SCREEN END OF SCREEN 1001.

*&---------------------------------------------------------------------*
*& AT SELECTION-SCREEN ON VALUE-REQUEST FOR p_fileo
*&---------------------------------------------------------------------*
AT SELECTION-SCREEN ON VALUE-REQUEST FOR p_fileo.

  PERFORM get_filename_download
    CHANGING
      p_fileo.

*&---------------------------------------------------------------------*
*&      Form  GET_FILENAME_DOWNLOAD
*&---------------------------------------------------------------------*
FORM get_filename_download
  CHANGING
    ch_v_file.

  DATA:
    lv_window_title     TYPE string,
    lv_default_filename TYPE string,
    lv_file_filter      TYPE string,
    lv_filename         TYPE string,
    lv_path             TYPE string,
    lv_fullpath         TYPE string,
    lv_selected_folder  TYPE string.

  CALL METHOD cl_gui_frontend_services=>directory_browse
    CHANGING
      selected_folder      = lv_selected_folder
    EXCEPTIONS
      cntl_error           = 1
      error_no_gui         = 2
      not_supported_by_gui = 3
      OTHERS               = 4.

  ch_v_file = lv_selected_folder.

ENDFORM.                    " GET_FILENAME_DOWNLOAD