*&---------------------------------------------------------------------*
*&  Include           YLN_ALV_TOP
*&---------------------------------------------------------------------*

*&---------------------------------------------------------------------*
*& TYPE-POOLS
*&---------------------------------------------------------------------*
TYPE-POOLS:
  slis,
  abap.

*&---------------------------------------------------------------------*
*& TABLES
*&---------------------------------------------------------------------*
TABLES:
  ekko,
  ekpo,
  rbkp.

*&---------------------------------------------------------------------*
*& TYPES
*&---------------------------------------------------------------------*
TYPES:
  BEGIN OF ty_ekko,
    ebeln TYPE ekko-ebeln,
    bukrs TYPE ekko-bukrs,
    bstyp TYPE ekko-bstyp,
    bsart TYPE ekko-bsart,
    lifnr TYPE ekko-lifnr,
    waers TYPE ekko-waers,
  END OF ty_ekko,

  tyt_ekko TYPE STANDARD TABLE OF ty_ekko.

TYPES:
  BEGIN OF ty_ekpo,
    ebeln TYPE ekpo-ebeln,
    ebelp TYPE ekpo-ebelp,
    matnr TYPE ekpo-matnr,
    werks TYPE ekpo-werks,
    lgort TYPE ekpo-lgort,
    menge TYPE ekpo-menge,
    meins TYPE ekpo-meins,
    netwr TYPE ekpo-netwr,
  END OF ty_ekpo,

  tyt_ekpo TYPE STANDARD TABLE OF ty_ekpo.

TYPES:
  BEGIN OF ty_rseg,
    belnr TYPE rseg-belnr,
    gjahr TYPE rseg-gjahr,
    buzei TYPE rseg-buzei,
    ebeln TYPE rseg-ebeln,
    ebelp TYPE rseg-ebelp,
  END OF ty_rseg,

  tyt_rseg TYPE STANDARD TABLE OF ty_rseg.

TYPES:
  BEGIN OF ty_rbkp,
    belnr TYPE rbkp-belnr,
    gjahr TYPE rbkp-gjahr,
    blart TYPE rbkp-blart,
    xblnr TYPE rbkp-xblnr,
    lifnr TYPE rbkp-lifnr,
  END OF ty_rbkp,

  tyt_rbkp TYPE STANDARD TABLE OF ty_rbkp.

TYPES:
  BEGIN OF ty_t161t,
    bsart TYPE t161t-bsart,
    bstyp TYPE t161t-bstyp,
    spras TYPE t161t-spras,
    batxt TYPE t161t-batxt,
  END OF ty_t161t,

  tyt_t161t TYPE STANDARD TABLE OF ty_t161t.

TYPES:
  BEGIN OF ty_makt,
    matnr TYPE makt-matnr,
    spras TYPE makt-spras,
    maktx TYPE makt-maktx,
  END OF ty_makt,

  tyt_makt TYPE STANDARD TABLE OF ty_makt.

TYPES:
  BEGIN OF ty_lfa1,
    lifnr TYPE lfa1-lifnr,
    name1 TYPE lfa1-name1,
  END OF ty_lfa1,

  tyt_lfa1 TYPE STANDARD TABLE OF ty_lfa1.

TYPES:
  ty_dd07v  TYPE dd07v,
  tyt_dd07v TYPE STANDARD TABLE OF ty_dd07v.

TYPES:
  BEGIN OF ty_alv,
    sel        TYPE abap_bool,
    ebeln      TYPE ekko-ebeln,     " Pedido de compras
    ebelp      TYPE ekpo-ebelp,     " Posici�n
    bukrs      TYPE ekko-bukrs,     " Sociedad
    bstyp      TYPE ekko-bstyp,     " Tipo documento
    bstyp_desc TYPE dd07t-ddtext,   " Descripci�n del Tipo de Documento
    bsart      TYPE ekko-bsart,     " Clase documento
    bsart_desc TYPE t161t-batxt,    " Descripci�n de la Clase de Documento
    lifnr      TYPE ekko-lifnr,     " Proveedor
    lifnr_name TYPE lfa1-name1,     " Nombre del Proveedor
    matnr      TYPE ekpo-matnr,     " Material
    matnr_desc TYPE makt-maktx,     " Descripci�n del Material
    werks      TYPE ekpo-werks,     " Centro
    lgort      TYPE ekpo-lgort,     " Almac�n
    menge      TYPE ekpo-menge,     " Cantidad
    meins      TYPE ekpo-meins,     " Unidad
    belnr      TYPE rbkp-belnr,     " Factura
    gjahr      TYPE rbkp-gjahr,     " Ejercicio
    blart      TYPE rbkp-blart,     " Clase de Doc.
    xblnr      TYPE rbkp-xblnr,     " Nro Legal
    netwr      TYPE ekpo-netwr,     " Importe
    waers      TYPE ekko-waers,     " Moneda
  END OF ty_alv,

  tyt_alv TYPE STANDARD TABLE OF ty_alv.

*&---------------------------------------------------------------------*
*& DATA
*&---------------------------------------------------------------------*
DATA:
  t_alv        TYPE tyt_alv.